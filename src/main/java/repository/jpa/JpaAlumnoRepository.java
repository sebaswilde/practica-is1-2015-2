package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.AlumnoRepository;
import domain.Alumno;

@Repository
public class JpaAlumnoRepository extends JpaBaseRepository<Alumno, Long> implements
		AlumnoRepository {

	@Override
	public Alumno findByApellidoPaterno(String apellidoPaterno) {
		//SELECT a.id, a.number, a.date FROM tbl_account a WHERE a.number = :number
		String jpaQuery = "SELECT a FROM Alumno a WHERE a.apellidoPaterno = :apellidoPaterno";
		TypedQuery<Alumno> query = entityManager.createQuery(jpaQuery, Alumno.class);
		query.setParameter("apellidoPaterno", apellidoPaterno);
		return getFirstResult(query);
	}
}