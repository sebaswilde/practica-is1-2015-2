package repository.jpa;

import java.util.Collection;

import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import repository.MatriculaRepository;
import domain.Account;
import domain.Matricula;

@Repository
public class JpaMatriculaRepository extends JpaBaseRepository<Matricula, Long> implements
MatriculaRepository {

	@Override
	public Collection<Matricula> findByCursoSemestre(String curso, String semestre) {
		String jpaQuery = "SELECT a FROM Matricula a JOIN a.alumno p JOIN a.curso c WHERE c.curso = :curso and a.semestre= :semestre";
		TypedQuery<Matricula> query = entityManager.createQuery(jpaQuery, Matricula.class);
		query.setParameter("curso", curso);
		query.setParameter("semestre", semestre);
		return query.getResultList();
	}

}