package repository;
import java.util.Collection;

import domain.Matricula;
public interface MatriculaRepository extends BaseRepository<Matricula, Long> {
	Collection<Matricula>  findByCursoSemestre(String curso, String semestre);
}