package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.MatriculaService;
import domain.Matricula;
import domain.Person;

@Controller
public class MatriculaController {
	
	@Autowired
	MatriculaService matriculaService;

	@RequestMapping(value = "/SubirNota", method = RequestMethod.GET)
	String addNewNota(@RequestParam(required = false) Long id, ModelMap model) {
		Matricula matricula = id == null ? new Matricula() : matriculaService.get(id);
		model.addAttribute("matricula", matricula);
		return "SubirNota";
	}
}