package controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import service.CursoService;
import domain.Curso;

@Controller
public class CursoController {
	
	@Autowired
	CursoService cursoService;

	@RequestMapping(value = "/curso", method = RequestMethod.POST)
	String savePerson(@ModelAttribute Curso curso, ModelMap model) {
		System.out.println("savving: " + curso.getId());
		cursoService.save(curso);
		return showPerson(curso.getId(), model);
	}

	@RequestMapping(value = "/curso", method = RequestMethod.GET)
	String showPerson(@RequestParam(required = false) Long id, ModelMap model) {
		if (id != null) {
			Curso curso = cursoService.get(id);
			model.addAttribute("curso", curso);
			return "curso";
		} else {
			Collection<Curso> cursos = cursoService.getAll();
			model.addAttribute("curso", cursos);
			return "curso";
		}
	}
}
